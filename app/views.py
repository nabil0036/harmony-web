from django.shortcuts import render
from .models import Recording
# Create your views here.

def home(request):
    return render(request,'app/home.html')

def product(request):
    if request.method == 'POST':
        audio_file = request.FILES['audio']
        c = Recording(audio = audio_file)
        c.save()
    return render(request,'app/product.html')